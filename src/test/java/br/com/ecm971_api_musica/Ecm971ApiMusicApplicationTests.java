package br.com.ecm971_api_musica;

import br.com.ecm971_api_musica.model.Music;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;


@SpringBootTest
@AutoConfigureMockMvc
public class Ecm971ApiMusicApplicationTests {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createMusicTest() throws Exception{
        String expectedValue = "[{\"id\":1,\"name\":\"Fire on the Water\",\"rating\":0}]";
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/music")
                        .param("name", "Fire on the Water")
        ).andExpect(status().isOk());
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get("/music")
                ).andExpect(status().isOk())
                .andExpect(content().json(expectedValue));

    }

    @Test
    public void ordinationTest() throws Exception{
        Music m1 = new Music(1L,"Undone thing",5);
        Music m2 = new Music(2L,"One",4);
        Music m3 = new Music(3L,"Still",3);
        Music m4 = new Music(4L,"Uncover",3);
        ArrayList<Music> musicList = new ArrayList<>();
        musicList.add(m1);
        musicList.add(m2);
        musicList.add(m3);
        musicList.add(m4);
        ArrayList<Music> sortedList = new ArrayList<>(musicList);
        String expectedValue = "[{\"id\":1,\"name\":\"Undone thing\",\"rating\":5},{\"id\":2,\"name\":\"One\",\"rating\":4},{\"id\":3,\"name\":\"Still\",\"rating\":3},{\"id\":4,\"name\":\"Uncover\",\"rating\":3}]";
        for (Music m:sortedList) {
            mockMvc.perform(
                    MockMvcRequestBuilders
                            .post("/music")
                            .param("name", m.getName())
            ).andExpect(status().isOk());

            mockMvc.perform(
                    MockMvcRequestBuilders
                            .put("/music")
                            .param("name", m.getName())
                            .param("rating", String.valueOf(m.getRating()))
            ).andExpect(status().isOk());

        }

        mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/music")
        ).andExpect(status().isOk())
                .andExpect(content().json(expectedValue));


    }

}
