package br.com.ecm971_api_musica.controller;

import br.com.ecm971_api_musica.model.Music;
import br.com.ecm971_api_musica.service.MusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/music")
public class MusicController {
    @Autowired
    private MusicService musicService;

    @PostMapping()
    public Music create (@RequestParam String name){
        boolean exists = this.musicService.getAll()
                .stream().map(Music::getName)
                .toList()
                .contains(name);
        if(!exists){
            Music music = new Music(null,name,0);
            this.musicService.create(music);
            return music;
        }else {
            return new Music(null,"music already exists",0);
        }

    }

    @PutMapping("/id")
    public void rateMusicById(@RequestParam int rating, Long id){
        if(rating >=1 && rating <=5){
            Optional<Music> music = this.musicService.getById(id);
            music.ifPresent(
                    value -> {
                        value.setRating(rating);
                        this.musicService.put(value);
                    }
            );
        }
    }
    @PutMapping()
    public String rateMusicByName(@RequestParam int rating, String name){
        if(rating >=1 && rating <=5){
            Optional<Music> music = this.musicService.getByName(name);
            music.ifPresent(
                    value -> {
                        value.setRating(rating);
                        this.musicService.put(value);
                    }

            );
            return music.get().toString();
        }
        return "Rating must be and integer between 1 and 5";
    }

    private final Comparator<Music> nameComparator =
            Comparator.comparing(Music::getName);
    @GetMapping
    public List<Music> getAll(){
        List<Music> allMusic = this.musicService.getAll();
        if(allMusic.size()>1){
            return allMusic.stream()
                    .sorted(
                            Comparator.comparingInt(Music::getRating)
                                    .thenComparing(nameComparator)
                    ).toList();
        }
        return allMusic;
    }
}
