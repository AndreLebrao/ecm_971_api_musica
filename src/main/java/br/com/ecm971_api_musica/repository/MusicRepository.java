package br.com.ecm971_api_musica.repository;

import br.com.ecm971_api_musica.model.Music;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MusicRepository extends JpaRepository<Music,Long> {
    Optional<Music> findByName(String name);
}
