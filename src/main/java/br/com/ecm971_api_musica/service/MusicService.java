package br.com.ecm971_api_musica.service;

import br.com.ecm971_api_musica.model.Music;
import br.com.ecm971_api_musica.repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MusicService {

    @Autowired
    private MusicRepository musicRepository;
    public void create (Music music){
        this.musicRepository.save(music);
    }
    public List<Music> getAll(){
        return this.musicRepository.findAll();
    }
    public Optional<Music> getById(Long id){ return this.musicRepository.findById(id);}
    public void put(Music music){this.musicRepository.save(music);}
    public Optional<Music> getByName(String name){ return this.musicRepository.findByName(name);}
}
