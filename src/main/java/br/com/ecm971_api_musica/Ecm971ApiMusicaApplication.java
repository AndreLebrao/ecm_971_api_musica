package br.com.ecm971_api_musica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ecm971ApiMusicaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ecm971ApiMusicaApplication.class, args);
    }

}
