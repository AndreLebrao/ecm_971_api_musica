package br.com.ecm971_api_musica.model;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//construtor padrão
@NoArgsConstructor
//construtor com todos os parâmetros, na ordem de declaração
@AllArgsConstructor
//quando aplicados a uma classe
//geram getters/setters para todos os campos não marcados como static
@Getter
@Setter
@Entity
public class Music {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private int rating;
}
